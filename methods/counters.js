Meteor.methods({
  countEligibleGodsons: function () {
    return Godsons.find({godFather: {$exists: false}}).count();
  }
});
