var app = angular.module('parrainage-esp-1516-main');

app.factory('CollectionResource',
  function ($meteor) {
    var CollectionResourceFactory = function (Collection, autobind) {
      var AngularCollection = $meteor.collection(Collection, autobind);
      return {
        query: function (selector) {
          if (!selector) {
            return AngularCollection;
          } else {
            return $meteor.collection(function () {
              return Collection.find(selector);
            });
          }
        },

        get: function (selector) {
          return $meteor.object(Collection, selector);
        },

        save: function (doc) {
          return AngularCollection.save(doc);
        },

        remove: function (selectorOrDoc) {
          return AngularCollection.remove(selectorOrDoc);
        }
      }
    }
    return CollectionResourceFactory;
  }
);
