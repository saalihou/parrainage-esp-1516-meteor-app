var app = angular.module('parrainage-esp-1516-main');

app.factory('Godfathers', function (CollectionResource) {
  return CollectionResource(Godfathers);
});
