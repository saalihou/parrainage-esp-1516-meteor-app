var app = angular.module('parrainage-esp-1516-main');

app.directive('pespForcedFocus',
  function ($window) {
    function link(scope, element, attrs) {
      element.focus();
      element.on('blur', function () {
        element.focus();
      });
    }
    return {
      link: link
    }
  }
);
