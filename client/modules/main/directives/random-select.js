var app = angular.module('parrainage-esp-1516-main');

app.directive('pespRandomSelect',
  function ($timeout, $interval) {
    function link (scope, element, attrs) {
      var trigger, interval, collection, iterations, model, illusionModel,
        predicate, eligibleItemsCount, selecting;
      trigger = attrs.pespRandomSelectTrigger;
      interval = parseInt(attrs.pespRandomSelectInterval) || 500;
      collection = attrs.pespRandomSelectCollection;
      iterations = parseInt(attrs.pespRandomSelectIterations) || 10;
      model = attrs.pespRandomSelectModel || 'selected';
      illusionModel = attrs.pespRandomSelectIllusionModel || 'fake-selected';
      predicate = scope[attrs.pespRandomSelectPredicate] || function () {
        return true;
      }
      eligibleItemsCountModel = attrs.pespRandomSelectEligibleCount;
      var eligibleItemsCount;
      scope.$watch(eligibleItemsCountModel, function (val) {
        eligibleItemsCount = val;
      });
      scope.$watch(trigger, function (val) {
        if (selecting) {
          return;
        }
        if (!val) {
          scope[illusionModel] = undefined;
          return;
        }
        selecting = true;
        var remainingIterations = iterations - 1;
        var intervalPromise = $interval(function () {
          remainingIterations--;
          var previousIllusion = scope[illusionModel];
          var randomIndex;

          do {
            randomIndex = _.random(scope[collection].length - 1);
            if (eligibleItemsCount <= 2) {
              remainingIterations = 0;
              break;
            }
          } while ( !predicate(scope[collection][randomIndex])
            || scope[collection][randomIndex] === previousIllusion
          );

          scope[illusionModel] = scope[collection][randomIndex];
          if (remainingIterations === 0) {
            $interval.cancel(intervalPromise);
            $timeout(function () {
              scope[illusionModel] = scope[model];
              selecting = false;
            }, interval)
          }
        }, interval);
      });
    }
    return {
      link: link
    }
  }
);
